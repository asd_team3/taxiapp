/**
 * Created by kote on 8.12.15.
 */
'use strict';

var app = angular.module('starter.auth', []);

app.factory('AuthEvents', [ function () {

  var service = {
    notAuthenticated: 401,
    notAuthorized: 403,
    sessionTimeout: 419,
    loginFailed: 401,
    loginSuccess: 200
  };

  return service;


}]);
