var app = angular.module('starter.controllers', ['starter.services']);

app.controller('BookingsCtrl', function($scope, $ionicModal, $http) {

  $scope.latitude = undefined;
  $scope.longitude = undefined;


  navigator.geolocation.getCurrentPosition(function(position) {


    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    var mapProp = {
      center: {lat: latitude, lng: longitude},
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('map'), mapProp);
    //$scope.marker = new google.maps.Marker(
    //  { map: $scope.map, position: mapProp.center});

    $scope.$apply(function (){
      $scope.latitude = latitude;
      $scope.longitude = longitude;
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('addr');
    var searchBox = new google.maps.places.SearchBox(input);
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      markers.forEach(function (marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function (place) {
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    })

    $('<div/>').addClass('centerMarker').appendTo(map.getDiv());

    google.maps.event.addListener(map, 'idle', function() {
      var center = map.getCenter();
      document.getElementById('lat').value = center.lat();
      document.getElementById('lng').value = center.lng();
      $scope.latitude = center.lat();
      $scope.longitude = center.lng();

    } )


  });

  $scope.sync_notification = '';

  $scope.submit = function() {
    $http.post('http://localhost:3000/bookings', {longitude: $scope.longitude, latitude: $scope.latitude})
      .then(function (response) {
        $scope.sync_notification = response.data.message;
        $scope.modal.show();
      });
  };

  $ionicModal.fromTemplateUrl('templates/bookings/sync-notification.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.closeModal = function() {
    $scope.sync_notification = '';
    $scope.modal.hide();
  };
});

app.controller('LoginController', function ($scope, AuthService, $location) {
  $scope.username = 'user1@example.com';
  $scope.password = 'user1234';
  $scope.submit = function() {
    var resp = AuthService.login($scope.username, $scope.password);
    $location.path('/bookings/new');
  };

});

app.controller('AppCtrl', function($scope, $ionicModal, $timeout, AuthService, $location) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    var resp = AuthService.login($scope.loginData.username, $scope.loginData.password);
    $location.path('/bookings/new');

    //var resp = AuthService.login($scope.loginData.username, $scope.loginData.password);
    //$location.path('/bookings/new');

  };
})

app.controller('MapController', function($scope, $ionicLoading) {





});
