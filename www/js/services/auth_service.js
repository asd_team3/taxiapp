/**
 * Created by kote on 8.12.15.
 */
'use strict';

var app = angular.module('starter.services', ['starter.token', 'starter.auth']);

app.factory("AuthService", function($http, $q, $rootScope, AuthToken, AuthEvents) {
    return {
        login: function(username, password) {
            var d = $q.defer();
            $http.post('http://localhost:3000/auth', {
                username: username,
                password: password
            }).success(function(resp) {
                AuthToken.set(resp.auth_token);
                $rootScope.$broadcast(AuthEvents.loginSuccess);
                d.resolve(resp.user);
            }).error(function(resp) {
                $rootScope.$broadcast(AuthEvents.loginFailed);
                d.reject(resp.error);
            });
            return d.promise;
        }
    };
});